/* -copyright-
#-# 
#-# Copyright (C) 1999-2001  Robin Hogan, 2021-2024 Willem Vermin
#-# 
#-# This program is free software; you can redistribute it and/or modify
#-# it under the terms of the GNU General Public License as published by
#-# the Free Software Foundation; either version 2 of the License, or
#-# (at your option) any later version.
#-# 
#-# This program is distributed in the hope that it will be useful,
#-# but WITHOUT ANY WARRANTY; without even the implied warranty of
#-# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#-# GNU General Public License for more details.
#-# 
#-# You should have received a copy of the GNU General Public License
#-# along with this program; if not, write to the Free Software
#-# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#-# 
*/
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <stdio.h>
#include <string.h>
#include <gtk/gtk.h>
#include <stdlib.h>
#include <X11/Intrinsic.h>
#include "utils.h"
#include "debug.h"
#include "toon.h"
#include "main.h"
#include "xpenguins.h"


Pixel Black, White;

FILE *HomeOpen(const char *file,const char *mode, char **path)
{
   char *h = getenv("HOME");
   if (h == NULL)
      return NULL;
   char *home = strdup(h);
   (*path) = (char *) malloc((strlen(home)+strlen(file)+2)*sizeof(char));
   strcpy(*path,home);
   strcat(*path,"/");
   strcat(*path,file);
   FILE *f = fopen(*path,mode);
   free(home);
   return f;
}


float sq3(float x, float y, float z)
{
   return x*x + y*y + z*z;
}

float sq2(float x, float y)
{
   return x*x + y*y;
}

float fsgnf(float x)
{
   if (x>0) return 1.0f;
   if (x<0) return -1.0f;
   return 0.0f;
}

int sgnf(float x)
{
   if (x > 0) return 1;
   if (x < 0) return -1;
   return 0;
}


int RandInt(int m)
{
   if (m <=0 )
      return 0;
   return drand48()*m;
}

int is_little_endian(void)
{
   int endiantest = 1;
   return (*(char *)&endiantest) == 1;
}

void my_cairo_paint_with_alpha(cairo_t *cr, double alpha)
{
   if (alpha > 0.9)
      cairo_paint(cr);
   else
      cairo_paint_with_alpha(cr,alpha);
   P("%d alpha %f\n",counter++,alpha);
}

/* from 'dsimple.c'
 * Window_With_Name: routine to locate a window with a given name on a display.
 *                   If no window with the given name is found, 0 is returned.
 *                   If more than one window has the given name, the first
 *                   one found will be returned.  Only top and its subwindows
 *                   are looked at.  Normally, top should be the RootWindow.
 */
Window Window_With_Name( Display *dpy, Window top, const char *name)
{
   Window *children, dummy;
   unsigned int nchildren;
   int i;
   Window w=0;
   char *window_name;

   // this leaks memory:
   //if (XFetchName(dpy, top, &window_name) && !strcmp(window_name, name))
   //  return(top);
   //  therefore:
   if (XFetchName(dpy, top, &window_name) && !strcmp(window_name, name))
   {
      XFree(window_name);
      return(top);
   }
   XFree(window_name);

   if (!XQueryTree(dpy, top, &dummy, &dummy, &children, &nchildren))
   {
      return(0);
   }

   for (i=0; (unsigned int)i<nchildren; i++) {
      w = Window_With_Name(dpy, children[i], name);
      if (w)
	 break;
   }
   if (children) XFree ((char *)children);
   return(w);
}


double wallcl() 
{ 
   return (double)g_get_real_time()*1.0e-6;
}

double wallclock()
{
   return (double)g_get_monotonic_time()*1.0e-6;
}


// usage of strncpy gives useless warning "-Wstringop-truncation",
// so here is a local version:
void mystrncpy(char *dest, const char *src, size_t n)
{
   // see if there is a null-byte within src. In that case we do a strcpy:
   size_t i;
   for (i = 0; i<n; i++)
   {
      if (src[i] == '\0')
      {
	 strcpy(dest, src);
	 P("strcpy\n");
	 return;
      }
   }
   // simply copy n chars:
   P("memcpy\n");
   memcpy(dest,src,n*sizeof(char));
}
