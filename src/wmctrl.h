/* -copyright-
#-# 
#-# Copyright (C) 1999-2001  Robin Hogan, 2021-2024 Willem Vermin
#-# 
#-# This program is free software; you can redistribute it and/or modify
#-# it under the terms of the GNU General Public License as published by
#-# the Free Software Foundation; either version 2 of the License, or
#-# (at your option) any later version.
#-# 
#-# This program is distributed in the hope that it will be useful,
#-# but WITHOUT ANY WARRANTY; without even the implied warranty of
#-# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#-# GNU General Public License for more details.
#-# 
#-# You should have received a copy of the GNU General Public License
#-# along with this program; if not, write to the Free Software
#-# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#-# 
*/
#pragma once
#include <X11/Xlib.h>
typedef struct _WinInfo
{
   Window id              ;
   int x,y                ; // x,y coordinates
   int xa,ya              ; // x,y coordinates absolute
   unsigned int w,h       ; // width, height
            int ws        ; // workspace

#ifdef NO_USE_BITS
   unsigned int sticky    ; // is visible on all workspaces
   unsigned int dock      ; // is a "dock" (panel)
   unsigned int hidden    ; // is hidden (iconified)
#else
   unsigned int sticky:  1; // is visible on all workspaces
   unsigned int dock  :  1; // is a "dock" (panel)
   unsigned int hidden:  1; // is hidden (iconified)
#endif
} WinInfo;

extern int          GetWindows(WinInfo **w, int *nw);

