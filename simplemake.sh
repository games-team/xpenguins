#!/bin/sh
# -copyright-
#-# 
#-# Copyright (C) 1999-2001  Robin Hogan, 2021-2024 Willem Vermin
#-# 
#-# This program is free software; you can redistribute it and/or modify
#-# it under the terms of the GNU General Public License as published by
#-# the Free Software Foundation; either version 2 of the License, or
#-# (at your option) any later version.
#-# 
#-# This program is distributed in the hope that it will be useful,
#-# but WITHOUT ANY WARRANTY; without even the implied warranty of
#-# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#-# GNU General Public License for more details.
#-# 
#-# You should have received a copy of the GNU General Public License
#-# along with this program; if not, write to the Free Software
#-# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#-# 
#
# This is a script which compiles xpenguins.
# Use and adapt this if the 
#   ./configure; make; make install
# suite does not work on your system
#

# Compilers:

# C compiler to compile .c sources:
CC=gcc    

# compile and link flags

FLAGS="-O2"
# if you have pkg-config working for gtk3:
FLAGS="$FLAGS `pkg-config --cflags --libs gtk+-3.0`"
# NOTE: on my system, pkg-config expands to:
# -pthread -I/usr/include/gtk-3.0 -I/usr/include/at-spi2-atk/2.0 -I/usr/include/at-spi-2.0 -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -I/usr/include/gtk-3.0 -I/usr/include/gio-unix-2.0 -I/usr/include/cairo -I/usr/include/pango-1.0 -I/usr/include/fribidi -I/usr/include/harfbuzz -I/usr/include/atk-1.0 -I/usr/include/cairo -I/usr/include/pixman-1 -I/usr/include/uuid -I/usr/include/freetype2 -I/usr/include/libpng16 -I/usr/include/gdk-pixbuf-2.0 -I/usr/include/libmount -I/usr/include/blkid -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -lgtk-3 -lgdk-3 -lpangocairo-1.0 -lpango-1.0 -lharfbuzz -latk-1.0 -lcairo-gobject -lcairo -lgdk_pixbuf-2.0 -lgio-2.0 -lgobject-2.0 -lglib-2.0


# if you have pkg-config working for gmodule-2.0:
#FLAGS="$FLAGS `pkg-config --cflags --libs gmodule-2.0`"
# NOTE: on my system, pkg-config expands to:
# -pthread -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -Wl,--export-dynamic -lgmodule-2.0 -pthread -lglib-2.0

# if you have pkg-config working for these: x11 xpm 
FLAGS="$FLAGS `pkg-config --cflags --libs x11 xt xpm xext`"
# NOTE: on my system, pkg-config expands to:
# -lXt -lXpm -lX11 -lXext

# link flags for libmath:
FLAGS="$FLAGS -lm"

# following is needed by gtk3 to recognize the buttons:
# (Should be delivered by pkg-config --cflags --libs gmodule-2.0)
FLAGS="$FLAGS -Wl,--export-dynamic"
# or:
# FLAGS="$FLAGS -rdynamic"

# Location of the resource files
# The following is for testing:
PKGDATADIR="`pwd`"

# If you want to have the theme files in a central location,
# uncomment one of the following definitions of PKGDATADIR
# and copy the theme files to that destination:
# e.g.:
# mkdir -p $HOME/.local/share/xpenguins
# cp -r themes $HOME/.local/share/xpenguins
# Note: the folowing command shows where xpenguins expects the themes:
#   xpenguins -h | grep System

# In your home directory:
#PKGDATADIR=$HOME/.local/share/xpenguins
# Alternatively, you can choose a system-wide installation:
#PKGDATADIR=/usr/local/share/xpenguins
#

FLAGS="$FLAGS -DPKGDATADIR=\"$PKGDATADIR\""

# If you want double buffering:
FLAGS="$FLAGS -DDOUBLE_BUFFER"

version=`./getversion`
if [ "x$version" = x ]; then
   version="Unknown"
fi

FLAGS="$FLAGS -DVERSION=\"$version\""

cd src || exit 1
echo "removing .o files :"
rm -f *.o

echo "creating ui_xml.h :"
./gen_ui_xml.sh  || exit 1

echo "compiling C sources:"
$CC -c *.c $FLAGS || exit 1

echo "creating xpenguins in directory $PWD:"
$CC -o xpenguins *.o $FLAGS || exit 1

cd ..
echo "creating man page xpenguins.1 in directory $PWD:"
version=`src/xpenguins -v|awk '{print $2}'`
sed "s'SYSTEMTHEMES'$PKGDATADIR/xpenguins/themes/*';s/VERSION/$version/;s/DATE/`date +'%B %Y'`/" < xpenguins.1.tmpl > xpenguins.1

echo
echo " ***********************************************************************"
echo " ** It seems that you compiled xpenguins successfully.                **"
echo " ** You can try to run it:                                            **"
echo " **                                                                   **"
echo " **    src/xpenguins                                                  **"
echo " **                                                                   **"
echo " ** If xpenguins works satisfactorily, you can install it:            **"
echo " **   Copy src/xpenguins to for example  /usr/local/bin/              **"
echo " ** Please have a look at the comments surrounding PKGDATADIR above.  **"
echo " **                                                                   **"
echo " ** Optionally, you can install the man page too:                     **"
echo " **   Copy src/xpenguins.1 to for example /usr/local/share/man/man1/  **"
echo " **                                                                   **"
echo " ** Optionally, you can install the desktop file and icon:            **"
echo " **   Copy src/xpenguins.desktop to for example                       **"
echo " **                 /usr/local/share/applications/                    **"
echo " **   Copy src/xpenguins.png to for example                           **"
echo " **                 /usr/local/share/pixmaps/                         **"
echo " ***********************************************************************"
